package ee.jwinters.dragongamebot;

import java.text.ParseException;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;

import ee.jwinters.dragongamebot.strategy.IStatsSelectionStrategy;
import ee.jwinters.dragongamebot.strategy.RandomStatsStrategy;
import ee.jwinters.dragongamebot.utils.InitLog4J;

/**
 * play one game over http
 * @author Joonatan
 */
public class IntegrationTest {

	@Before
	public void init() {
		InitLog4J.initLogging();
	}
	
	@Test
	public void testWithRandomStats() throws JsonProcessingException, ParseException, InterruptedException {
		AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(DragonBotHttpConfig.class);
		IStatsSelectionStrategy strategy = new RandomStatsStrategy();
		DragonBotsRunner.playGames(1, false, strategy, applicationContext);
	}
	
}

