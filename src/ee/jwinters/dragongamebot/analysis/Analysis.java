package ee.jwinters.dragongamebot.analysis;

import static ee.jwinters.dragongamebot.msg.Weather.DRY;
import static ee.jwinters.dragongamebot.msg.Weather.FOG;
import static ee.jwinters.dragongamebot.msg.Weather.NORMAL;
import static ee.jwinters.dragongamebot.msg.Weather.RAIN;
import static ee.jwinters.dragongamebot.msg.Weather.STORM;
import static ee.jwinters.dragongamebot.utils.ExceptionFunction.rethrow;
import static ee.jwinters.dragongamebot.utils.PredicateSplitterConsumer.whenThenElse;
import static ee.jwinters.dragongamebot.utils.TrainingDataUtils.getTimeStamp;
import static java.util.Arrays.asList;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.apache.log4j.Logger;

import ee.jwinters.dragongamebot.msg.Weather;
import ee.jwinters.dragongamebot.strategy.TrainingData;
import ee.jwinters.dragongamebot.utils.InitLog4J;
import ee.jwinters.dragongamebot.utils.PredicateSplitterConsumer;
import ee.jwinters.dragongamebot.utils.TrainingDataUtils;

/**
 * splitting training data by weather
 * @author Joonatan
 *
 */
public class Analysis {
	
	public static int COUNT = 0;

	static Logger LOG = Logger.getLogger(Analysis.class);
	
	public static final String WEATHER_NORMAL_FILENAME = "normal_weather_data.txt";
	public static final String WEATHER_STORM_FILENAME = "storm_weather_data.txt";
	public static final String WEATHER_DRY_FILENAME = "dry_weather_data.txt";
	public static final String WEATHER_RAIN_FILENAME = "rain_weather_data.txt";
	public static final String WEATHER_FOG_FILENAME = "fog_weather_data.txt";
	
	static private Map<Weather,String> weatherToFileMap = new HashMap<>();
	static {
		weatherToFileMap.put(DRY, WEATHER_DRY_FILENAME);
		weatherToFileMap.put(FOG, WEATHER_FOG_FILENAME);
		weatherToFileMap.put(NORMAL, WEATHER_NORMAL_FILENAME);
		weatherToFileMap.put(RAIN, WEATHER_RAIN_FILENAME);
		weatherToFileMap.put(STORM, WEATHER_STORM_FILENAME);
	}

	public static void splitWeather(Stream<File> inputs) {
		File folder = new File("./analysis/weather_split_"+getTimeStamp());
		folder.mkdirs();
		folder.mkdir();
		splitWeather(folder, inputs);
	}
	
	public static void splitWeather(File folder, Stream<File> inputs) {
		folder.mkdirs();
		
		Consumer<String> ignoreLine = (line)->{
			LOG.info("ignored line: "+line);
		};
		
		PredicateSplitterConsumer<String> outputStorm = whenThenElse(is(STORM), output(STORM, folder),ignoreLine);
		PredicateSplitterConsumer<String> outputRain = whenThenElse(is(RAIN), output(RAIN, folder),outputStorm);
		PredicateSplitterConsumer<String> outputNormal = whenThenElse(is(NORMAL), output(NORMAL, folder),outputRain);
		PredicateSplitterConsumer<String> outputFog = whenThenElse(is(FOG), output(FOG, folder),outputNormal);
		PredicateSplitterConsumer<String> outputDry = whenThenElse(is(DRY), output(DRY, folder),outputFog);
		
		PredicateSplitterConsumer<String> splitAndOutput = outputDry;
		inputs
			.map(File::toPath)
			.flatMap(rethrow(Files::lines))
			.forEach(splitAndOutput);;
	}

	private static Predicate<String> is(Weather weather) {
		return (s)->checkWeather(s,weather);
	}

	private static boolean checkWeather(String trainingDtaLine, Weather weather) {
		TrainingData dataLine = TrainingDataUtils.parseTrainingLine(trainingDtaLine);
		return dataLine.weather == weather;
	}

	private static Consumer<String> output(Weather fileType, File folder) {
		File file = new File(folder,weatherToFileMap.get(fileType));
		return (line)->{
			try {
				TrainingDataUtils.appendLineToFile(file,line);
				LOG.info("analysis progress: "+(++COUNT)+" lines");
			} catch (IOException e) {
				LOG.error("error writing to analysis folder", e);
				throw new RuntimeException(e);
			}
		};
	}
	
	public static void main(String[] args) {
		InitLog4J.initLogging();
		
		/*String bigTestFile = ".//bak//trainingData_300K.txt";
		String mainlyFogFile = ".//bak//trainingData_mainly_fog.txt";
		List<String> fileNameStrings = asList(bigTestFile,mainlyFogFile);*/
		
		List<String> fileNameStrings = asList(
			".//bak//simulated_train_data//all_data_extra.txt",
			".//bak//simulated_train_data//data_2.txt",
			".//bak//simulated_train_data//data_mainly_fog.txt"
		);
		
		Stream<String> fileNames = fileNameStrings.stream();
		Stream<File> files = fileNames.map(File::new);
		Analysis.splitWeather(files);
	}


}
