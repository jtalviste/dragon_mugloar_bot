package ee.jwinters.dragongamebot.rest;

import java.text.ParseException;

import ee.jwinters.dragongamebot.msg.Weather;

/**
 * A weather API client to get weather during a fight 
 * @author Joonatan
 */
public interface IWeatherClient {

	/**
	 * Gets weather for game
	 * @param gameId - game ID that the weather is for 
	 * @return
	 * @throws ParseException
	 */
	Weather getWeather(long gameId) throws ParseException;

}