package ee.jwinters.dragongamebot.rest;

import static ee.jwinters.dragongamebot.msg.Weather.toWeather;

import java.text.ParseException;

import org.apache.log4j.Logger;
import org.springframework.web.client.RestOperations;

import ee.jwinters.dragongamebot.msg.Weather;
import ee.jwinters.dragongamebot.msg.WeatherResponse;
import ee.jwinters.dragongamebot.utils.JSONUtils;

/**
 * Weather API client using HTTP/REST 
 * @author Joonatan
 */
public class WeatherHttpClient implements IWeatherClient {
	
	static private Logger LOG = Logger.getLogger(WeatherHttpClient.class);

	private RestOperations restTemplate;

	public WeatherHttpClient(RestOperations restTemplate) {
		this.restTemplate = restTemplate;
	}

	/* (non-Javadoc)
	 * @see ee.jwinters.dragongamebot.rest.IWeatherClient#getWeather(long)
	 */
	@Override
	public Weather getWeather(long gameId) throws ParseException {
		String weatherUrl = "http://www.dragonsofmugloar.com/weather/api/report/{gameId}";
		WeatherResponse response = restTemplate.getForObject(weatherUrl, WeatherResponse.class, gameId);
		LOG.debug("Got weather response " + JSONUtils.toFormattedJSON(response));
		return toWeather(response);
	}

}
