package ee.jwinters.dragongamebot.rest;

import com.fasterxml.jackson.core.JsonProcessingException;

import ee.jwinters.dragongamebot.msg.DragonStatsSolution;
import ee.jwinters.dragongamebot.msg.GameCreateResponse;
import ee.jwinters.dragongamebot.msg.GameResultResponse;

/**
 * Client that allows one to play dragon games
 * @author Joonatan
 *
 */
public interface IDragonGameClient {
	
	/**
	 * create a game to play 
	 * @return
	 */
	GameCreateResponse createGame();

	/**
	 * send dragon to fight and get game results
	 * @param gameId - game to play, got with GameCreateResponse during createGame()
	 * @param solution - result of game
	 * @return
	 * @throws JsonProcessingException
	 */
	GameResultResponse playGame(long gameId, DragonStatsSolution solution) throws JsonProcessingException;

}