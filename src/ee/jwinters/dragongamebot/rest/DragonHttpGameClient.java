package ee.jwinters.dragongamebot.rest;

import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestOperations;

import com.fasterxml.jackson.core.JsonProcessingException;

import ee.jwinters.dragongamebot.msg.DragonStatsSolution;
import ee.jwinters.dragongamebot.msg.GameCreateResponse;
import ee.jwinters.dragongamebot.msg.GameResultResponse;
import ee.jwinters.dragongamebot.utils.JSONUtils;

/**
 * REST/HTTP cliet for the dragon game
 * @author Joonatan
 *
 */
public class DragonHttpGameClient implements IDragonGameClient {
	
	static private Logger LOG = Logger.getLogger(DragonHttpGameClient.class);

	private RestOperations restTemplate;

	/**
	 * initialize a game client with Spring RestTemplate
	 * @param restTemplate
	 */
	public DragonHttpGameClient(RestOperations restTemplate) {
		this.restTemplate = restTemplate;
	}

	/* (non-Javadoc)
	 * @see ee.jwinters.dragongamebot.rest.IDragonGameClient#createGame()
	 */
	@Override
	public GameCreateResponse createGame() {
		String gameCreateUrl = "http://www.dragonsofmugloar.com/api/game";
		GameCreateResponse response = restTemplate.getForObject(gameCreateUrl, GameCreateResponse.class);
		LOG.debug("Got game create response " + JSONUtils.toFormattedJSON(response));
		return response;
	}

	/* (non-Javadoc)
	 * @see ee.jwinters.dragongamebot.rest.IDragonGameClient#playGame(long, ee.jwinters.dragongamebot.msg.DragonStatsSolution)
	 */
	@Override
	public GameResultResponse playGame(long gameId, DragonStatsSolution solution) throws JsonProcessingException {
	    HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON); 
	    HttpEntity<DragonStatsSolution> entity = new HttpEntity<DragonStatsSolution>(solution, headers);
	    
		String url = "http://www.dragonsofmugloar.com/api/game/{gameId}/solution";
		//String url = "http://jwinters.requestcatcher.com/test";
		ResponseEntity<GameResultResponse> ret = restTemplate.exchange(url, HttpMethod.PUT, entity, GameResultResponse.class, gameId);
		return ret.getBody();
	}

}
