package ee.jwinters.dragongamebot.utils;

import static java.util.Calendar.DAY_OF_MONTH;
import static java.util.Calendar.HOUR_OF_DAY;
import static java.util.Calendar.MILLISECOND;
import static java.util.Calendar.MINUTE;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.SECOND;
import static java.util.Calendar.YEAR;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Calendar;
import java.util.Comparator;

import ee.jwinters.dragongamebot.msg.Dragon;
import ee.jwinters.dragongamebot.msg.Knight;
import ee.jwinters.dragongamebot.msg.Weather;
import ee.jwinters.dragongamebot.strategy.TrainingData;

/**
 * Utilities to save training data - won games
 * @author Joonatan
 * 
 */
public class TrainingDataUtils {

	//one data file per session
	public static File CURRENT_TRAINING_FILE;
	static {
		String timestamp = getTimeStamp();
		String filename = "trainingData" + timestamp + ".txt";
		CURRENT_TRAINING_FILE = new File("trainingdata/" + filename);
	}

	/**
	 * get a uniquie timestamp
	 * @return
	 */
	public static String getTimeStamp() {
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(YEAR);
		int month = calendar.get(MONTH) + 1;
		int day = calendar.get(DAY_OF_MONTH);
		int hours = calendar.get(HOUR_OF_DAY);
		int minutes = calendar.get(MINUTE);
		int seconds = calendar.get(SECOND);
		int milliSeconds = calendar.get(MILLISECOND);
		String timestamp = "" + year + "_" + month + "_" + day + "_" + hours + "_" + minutes + "_" + seconds + "_"
				+ milliSeconds;
		return timestamp;
	}

	/**
	 * mark training data file that its simulated
	 * @throws IOException
	 */
	public static void markSimulated() throws IOException {
		File file = CURRENT_TRAINING_FILE;
		File folder = file.getParentFile();
		String newName = "simulated_" + file.getName();
		CURRENT_TRAINING_FILE = new File(folder, newName);
	}

	/**
	 * update the training data file with a winning game data
	 * @param knight - the game's knight
	 * @param weather - weather for game
	 * @param dragon - dragon used
	 * @throws IOException
	 */
	public static synchronized void saveGameWin(Knight knight, Weather weather, Dragon dragon) throws IOException {
		/*if (weather == Weather.FOG) {
			return;// just creates noise
		}*/

		String line = makeTrainingDataLine(knight, weather, dragon);
		
		appendLineToFile(CURRENT_TRAINING_FILE, line);
	}

	public static void appendLineToFile(File outputFile, String line) throws IOException, FileNotFoundException {
		checkThatFileExists(outputFile);
		try (// resource using block
				FileOutputStream fos = new FileOutputStream(outputFile, true);
				PrintWriter out = new PrintWriter(fos)) {
			out.println(line);
		}
	}

	/**
	 * create a line of training data
	 * @param knight - the game's knight
	 * @param weather - weather for game
	 * @param dragon - dragon used
	 * @return
	 */
	public static String makeTrainingDataLine(Knight knight, Weather weather, Dragon dragon) {
		String[] inElements = { "" + knight.agility, "" + knight.armor, "" + knight.attack, "" + knight.endurance,
				"" + weather.isNormal(), "" + weather.isStorm(), "" + weather.isRain(), "" + weather.isDry(),
				"" + weather.isFog() };
		String[] stats = { "" + dragon.clawSharpness, "" + dragon.fireBreath, "" + dragon.scaleThickness,
				"" + dragon.wingStrength, };

		String input = String.join(",", inElements);
		String output = String.join(",", stats);
		String outputLine = input + ";" + output;
		return outputLine;
	}
	
	/**
	 * read a training data line and make object
	 * @param line
	 * @return
	 */
	public static TrainingData parseTrainingLine(String line) {
		String[] inputAndOutput = line.split(";");
		String input = inputAndOutput[0];
		String output = inputAndOutput[1];
		String[] inElements = input.split(",");
		String[] stats = output.split(","); 
		TrainingData ret = new TrainingData(toKnight(inElements),toWeather(inElements),toDragon(stats));
		return ret;
	}

	/**
	 * parse training data knight
	 * @param inElements
	 * @return
	 */
	private static Knight toKnight(String[] inElements) {
		int agility = Integer.parseInt(inElements[0]);
		int armor = Integer.parseInt(inElements[1]);
		int attack = Integer.parseInt(inElements[2]);
		int endurance = Integer.parseInt(inElements[3]);
		return new Knight(agility, armor, attack, endurance);
	}

	/**
	 * parse training data weather
	 * @param inElements
	 * @return
	 */
	private static Weather toWeather(String[] inElements) {
		if(inElements[4].equals("1")) return Weather.NORMAL;
		if(inElements[5].equals("1")) return Weather.STORM;
		if(inElements[6].equals("1")) return Weather.RAIN;
		if(inElements[7].equals("1")) return Weather.DRY;
		if(inElements[8].equals("1")) return Weather.FOG;
		throw new RuntimeException("invalid training line");
	}

	/**
	 * parse training data dragon
	 * @param stats
	 * @return
	 */
	private static Dragon toDragon(String[] stats) {
		int clawSharpness = Integer.parseInt(stats[0]);
		int fireBreath = Integer.parseInt(stats[1]);
		int scaleThickness = Integer.parseInt(stats[2]);
		int wingStrength = Integer.parseInt(stats[3]);
		return new Dragon(clawSharpness, fireBreath, scaleThickness, wingStrength);
	}
	
	public static void checkThatFileExists(File file) throws IOException {
		if (!file.exists()) {
			file.getParentFile().mkdirs();
			if(file.isDirectory()) {
				file.mkdir();
			} else {
				file.createNewFile();				
			}
		}
	}

	/**
	 * delete any file or folder
	 * @param folder
	 * @throws IOException
	 */
	public static void deleteFolder(File folder) throws IOException {
		if(!folder.exists()) {
			return;
		}
		if(folder.isFile()) {
			folder.delete();
			return;
		}
		Path dirPath = folder.toPath();
		Files.walk(dirPath).map(Path::toFile).sorted(Comparator.comparing(File::isDirectory)).forEach(File::delete);
	}

}