package ee.jwinters.dragongamebot.utils;

import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * utility class to split streams
 * @author Joonatan
 *
 * @param <T>
 */
public class PredicateSplitterConsumer<T> implements Consumer<T> {

	public static <T> PredicateSplitterConsumer<T> whenThenElse(Predicate<T> predicate, Consumer<T> positive, Consumer<T> negative){
		return new PredicateSplitterConsumer<>(predicate, positive, negative);
	}
	
	private Predicate<T> predicate;
	private Consumer<T> positiveConsumer;
	private Consumer<T> negativeConsumer;

	/**
	 * @param predicate - used to split the stream
	 * @param positive - consumer of positive elements
	 * @param negative - consumer of negative elements
	 */
	public PredicateSplitterConsumer(Predicate<T> predicate, Consumer<T> positive, Consumer<T> negative) {
		this.predicate = predicate;
		this.positiveConsumer = positive;
		this.negativeConsumer = negative;
	}

	@Override
	public void accept(T t) {
		if (predicate.test(t)) {
			positiveConsumer.accept(t);
		} else {
			negativeConsumer.accept(t);
		}
	}
}