package ee.jwinters.dragongamebot.utils;

import java.io.PrintStream;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.Priority;

/**
 * log4j progrmmatic config
 * @author Joonatan
 *
 */
public class InitLog4J {
	/**
	 * initializes log4j
	 */
	public static void initLogging() {
		PatternLayout layout = new PatternLayout("%d{HH:mm:ss,SSS} %-6r [%15.15t] %-5p %30.30c %x - %m%n");
		
		Logger rootLogger = Logger.getRootLogger();	    
	    FileAppender fileAppender = new FileAppender();
	    fileAppender.setName("log all to default file");
	    fileAppender.setFile("log.txt");
	    fileAppender.setThreshold(Level.ALL);
		fileAppender.setLayout(layout);
	    fileAppender.setAppend(true);
	    fileAppender.activateOptions();
	    rootLogger.addAppender(fileAppender);
	    
	    ConsoleAppender console = new ConsoleAppender(); //create appender
	    //configure the appender
	    console.setLayout(layout); 
	    console.setThreshold(Level.INFO);
	    console.activateOptions();
	    //add appender to any Logger (here is root)
	    Logger.getRootLogger().addAppender(console);
	    
	    tieSystemOutAndErrToLog();
	}
	
	/**
	 * creates a duplicate of a printstream to log4j log 
	 * @param realPrintStream
	 * @param prio
	 * @return
	 */
	private static PrintStream createLoggingProxy(final PrintStream realPrintStream, Priority prio) {
        return new PrintStream(realPrintStream) {
            public void print(final String string) {
                realPrintStream.print(string);
				Logger.getRootLogger().log(prio, string);
            }
        };
    }
	
	private static void tieSystemOutAndErrToLog() {
        System.setOut(createLoggingProxy(System.out, Level.WARN));
        System.setErr(createLoggingProxy(System.err, Level.ERROR));
    }
}
