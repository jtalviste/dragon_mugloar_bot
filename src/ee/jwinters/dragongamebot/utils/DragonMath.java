package ee.jwinters.dragongamebot.utils;

import static java.lang.Math.random;

import ee.jwinters.dragongamebot.msg.Dragon;
import ee.jwinters.dragongamebot.msg.DragonStatsSolution;
import ee.jwinters.dragongamebot.msg.Knight;
import ee.jwinters.dragongamebot.msg.Weather;
/**
 * math functions
 * @author Joonatan
 *
 */
public class DragonMath {

	/**
	 * make dragon stats add up to 20
	 * @param scaleThicknessD
	 * @param clawSharpnessD
	 * @param wingStrengthD
	 * @param fireBreathD
	 * @return
	 */
	public static DragonStatsSolution normalizeDragonSolution(double scaleThicknessD, double clawSharpnessD, double wingStrengthD, double fireBreathD) {
		Dragon dragon = normalizeDragon(scaleThicknessD, clawSharpnessD, wingStrengthD, fireBreathD);
		return new DragonStatsSolution(dragon);
	}

	public static Dragon normalizeDragon(double scaleThicknessD, double clawSharpnessD, double wingStrengthD,
			double fireBreathD) {
		int[] stats = normalizeStats(scaleThicknessD, clawSharpnessD, wingStrengthD, fireBreathD);
		Dragon dragon = new Dragon(stats[0], stats[1], stats[2], stats[3]);
		return dragon;
	}

	/**
	 * make dragon stats add up to 20
	 * @param stat0D
	 * @param stat1D
	 * @param stat2D
	 * @param stat3D
	 * @return - array of stats
	 */
	public static int[] normalizeStats(double stat0D, double stat1D, double stat2D, double stat3D) {
		double sum = stat0D + stat1D + stat2D + stat3D;
		double N = 20;
		int stat0 = (int) (stat0D / sum * N) ;
		int stat1 = (int) ((stat0D+stat1D) / sum * N) - stat0;
		int stat2 = (int) ((stat0D+stat1D+stat2D) / sum * N) - stat1 - stat0;
		int stat3 = 20 - stat2 - stat1 - stat0;
		int[] stats = {stat0, stat1, stat2, stat3};
		return stats;
	}

	/**
	 * multiply a vector with a matrix
	 * @param vector
	 * @param matrix
	 * @return
	 */
	public static double[] multVectorMatrix(double[] vector, double[][] matrix) {
		int vectorLength = vector.length;
		double[] ret = new double[vectorLength];
		for (int matrixRow = 0; matrixRow < matrix.length; matrixRow++) {
			for (int matrixCol = 0; matrixCol < matrix[matrixRow].length; matrixCol++) {
				ret[matrixCol] += vector[matrixRow] * matrix[matrixRow][matrixCol];
			}
		}
		return ret;
	}

	public static int randomInRange(int length) {
		return (int)(Math.random()*length);
	}

	public static Knight randomKnight() {
		int[] arr = normalizeStats(random(), random(), random(), random());
		return new Knight(arr[0],arr[1],arr[2],arr[3]);
	}

	public static Weather randomWeather() {
		Weather[] weatherTypes = Weather.values();
		int n = weatherTypes.length;
		int index = randomInRange(n);
		return weatherTypes[index];
	}

	public static Dragon randomDragon() {
		double scaleThicknessD = random();
		double clawSharpnessD = random();
		double wingStrengthD = random();
		double fireBreathD = random();
		
		Dragon dragon = normalizeDragon(scaleThicknessD, clawSharpnessD, wingStrengthD, fireBreathD);
		return dragon;
	}

}
