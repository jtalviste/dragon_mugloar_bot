package ee.jwinters.dragongamebot.utils;

import java.util.function.Function;

/**
 * Function interface that allows exceptions
 * @author Joonatan
 *
 * @param <T>
 * @param <R>
 */
@FunctionalInterface
public interface ExceptionFunction<T, R> {

    /**
     * Applies this function to the given argument.
     *
     * @param t the function argument
     * @return the function result
     */
    R apply(T t) throws Exception;

	public static <T, R> Function<T, R> rethrow(ExceptionFunction<T, R> function) {
		return (arguments) -> {
			try {
				return function.apply(arguments);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		};
	}
    
}