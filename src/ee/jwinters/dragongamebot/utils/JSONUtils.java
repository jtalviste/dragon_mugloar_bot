package ee.jwinters.dragongamebot.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
 
/**
 * JSON utilities
 * @author Joonatan
 *
 */
public class JSONUtils {
	private static ObjectMapper objectMapper = new ObjectMapper();

	/**
	 * serialize an object to formatted JSON
	 * @param object
	 * @return
	 */
	public static String toFormattedJSON(Object object) {
		try {
			return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	/**
	 * serialize an object to regular JSON
	 * @param object
	 * @return
	 */
	public static String toJson(Object object) throws JsonProcessingException {
		String jsonOutString = objectMapper.writeValueAsString(object);
		return jsonOutString;
	}

}
