/*
 * Copyright 2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ee.jwinters.dragongamebot;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;

import ee.jwinters.dragongamebot.rest.IDragonGameClient;
import ee.jwinters.dragongamebot.rest.IWeatherClient;
import ee.jwinters.dragongamebot.strategy.IStatsSelectionStrategy;

/**
 * main application logging
 * @author Joonatan
 *
 */
public class DragonBotsRunner {
	
	static Logger LOG = Logger.getLogger(DragonBotsRunner.class);
	
	/**
	 * 
	 * @param numberOfGames - how many games to play
	 * @param train - true if saving training data
	 * @param strategy - the strategy to use when playing
	 * @param applicationContext - spring application context to use to create beans
	 * @throws JsonProcessingException
	 * @throws InterruptedException
	 */
	public static void playGames(int numberOfGames, boolean train, IStatsSelectionStrategy strategy,
			AnnotationConfigApplicationContext applicationContext) throws JsonProcessingException, InterruptedException {
		GameStats stats = new GameStats(numberOfGames, train);
		IDragonGameClient gameClient = applicationContext.getBean("gameClient", IDragonGameClient.class);
        IWeatherClient weatherClient = applicationContext.getBean("weatherClient", IWeatherClient.class);
        applicationContext.close();
        
        ExecutorService playTaskExecutor = Executors.newFixedThreadPool(200);
		for (int i = 0; i < numberOfGames; i++) {
			playTaskExecutor.submit(new PlayGameTask(stats, gameClient, weatherClient, strategy));
		}
        playTaskExecutor.shutdown();
        LOG.debug(train);
        
        while (!playTaskExecutor.isTerminated()){
			stats.printInfo();
			Thread.sleep(1000);
		} 
        stats.printInfo();
	}

}
