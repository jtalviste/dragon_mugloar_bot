package ee.jwinters.dragongamebot.strategy;

import ee.jwinters.dragongamebot.msg.Dragon;
import ee.jwinters.dragongamebot.msg.DragonStatsSolution;
import ee.jwinters.dragongamebot.msg.Knight;
import ee.jwinters.dragongamebot.msg.Weather;

/**
 * a strategy that always uses the same dragon
 * @author Joonatan
 *
 */
public class FixedStrategy implements IStatsSelectionStrategy {

	private int clawSharpness;
	private int fireBreath;
	private int scaleThickness;
	private int wingStrength;
	
	

	public FixedStrategy(int clawSharpness, int fireBreath, int scaleThickness, int wingStrength) {
		super();
		this.clawSharpness = clawSharpness;
		this.fireBreath = fireBreath;
		this.scaleThickness = scaleThickness;
		this.wingStrength = wingStrength;
	}

	@Override
	public DragonStatsSolution solve(Knight knight, Weather weatherForGame) {
		return new DragonStatsSolution(new Dragon(clawSharpness, fireBreath, scaleThickness, wingStrength));
	}

}
