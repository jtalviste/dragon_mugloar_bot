package ee.jwinters.dragongamebot.strategy;

import ee.jwinters.dragongamebot.msg.Dragon;
import ee.jwinters.dragongamebot.msg.Knight;
import ee.jwinters.dragongamebot.msg.Weather;

/**
 * object representing a line of training data
 * @author Joonatan
 *
 */
public class TrainingData {

	public Knight knight;
	public Weather weather;
	public Dragon dragon;
	
	public TrainingData(Knight knight, Weather weather, Dragon dragon) {
		super();
		this.knight = knight;
		this.weather = weather;
		this.dragon = dragon;
	}
	
}
