package ee.jwinters.dragongamebot.strategy;

import ee.jwinters.dragongamebot.msg.DragonStatsSolution;
import ee.jwinters.dragongamebot.msg.Knight;
import ee.jwinters.dragongamebot.msg.Weather;

/**
 * Stats selection strategy that varies with weather - different strategy used for each weather. 
 * @author Joonatan
 */
public class WeatherStrategy implements IStatsSelectionStrategy {

	private IStatsSelectionStrategy normal;
	private IStatsSelectionStrategy storm;
	private IStatsSelectionStrategy rain;
	private IStatsSelectionStrategy dry;
	private IStatsSelectionStrategy fog;

	/**
	 * initialize with strategies for each weather
	 * @param normal
	 * @param storm
	 * @param rain
	 * @param dry
	 * @param fog
	 */
	public WeatherStrategy(IStatsSelectionStrategy normal, IStatsSelectionStrategy storm, IStatsSelectionStrategy rain,
			IStatsSelectionStrategy dry, IStatsSelectionStrategy fog) {
		this.normal = normal;
		this.storm = storm;
		this.rain = rain;
		this.dry = dry;
		this.fog = fog;
	}

	/**
	 * select stats for game
	 */
	@Override
	public DragonStatsSolution solve(Knight knight, Weather weatherForGame) {
		switch (weatherForGame) {
			case NORMAL: return normal.solve(knight, weatherForGame);
			case STORM: return storm.solve(knight, weatherForGame);
			case RAIN: return rain.solve(knight, weatherForGame);
			case DRY: return dry.solve(knight, weatherForGame);
			case FOG: return fog.solve(knight, weatherForGame);
		}
		throw new NullPointerException("no strategy for weather "+weatherForGame);
	}

}
