package ee.jwinters.dragongamebot.strategy;

import ee.jwinters.dragongamebot.msg.Dragon;
import ee.jwinters.dragongamebot.msg.DragonStatsSolution;
import ee.jwinters.dragongamebot.msg.Knight;
import ee.jwinters.dragongamebot.msg.Weather;
import ee.jwinters.dragongamebot.utils.DragonMath;

/**
 * Strategy that selects dragon stats randomly
 * @author Joonatan
 *
 */
public class RandomStatsStrategy implements IStatsSelectionStrategy {
	
	/**
	 * play a game randomly
	 */
	@Override
	public DragonStatsSolution solve(Knight knight, Weather weather) {
		return randomSolution();
	}

	public DragonStatsSolution randomSolution() {
		Dragon dragon = DragonMath.randomDragon();
		return new DragonStatsSolution(dragon);
	}
	
	/*
	@Override
	public DragonStatsSolution solve(Knight knight, Weather weather) {
		double scaleThicknessD = random();
		double clawSharpnessD = random();
		double wingStrengthD = random();
		double fireBreathD = random();
		
		scaleThicknessD += knight.attack;
		
		clawSharpnessD += knight.armor;
		
		fireBreathD += 4;
		fireBreathD += knight.armor / 1.5;
		fireBreathD += knight.endurance / 5;
		
		wingStrengthD += 1;
		
		if(weather == Weather.RAIN) {
			scaleThicknessD = random();
			clawSharpnessD = random();
			wingStrengthD = random();
			fireBreathD = random();
		}
		
		//try not to overdose on drugs
		double sum = scaleThicknessD + clawSharpnessD + wingStrengthD + fireBreathD;
		double max = 9 * sum / 20d;
		scaleThicknessD = Math.min(scaleThicknessD, max);
		clawSharpnessD = Math.min(clawSharpnessD, max);
		wingStrengthD = Math.min(wingStrengthD, max);
		fireBreathD = Math.min(fireBreathD, max);
		
		sum = scaleThicknessD + clawSharpnessD + wingStrengthD + fireBreathD;
		max = 9 * sum / 20d;
		scaleThicknessD = Math.min(scaleThicknessD, max);
		clawSharpnessD = Math.min(clawSharpnessD, max);
		wingStrengthD = Math.min(wingStrengthD, max);
		fireBreathD = Math.min(fireBreathD, max);
		
		sum = scaleThicknessD + clawSharpnessD + wingStrengthD + fireBreathD;
		max = 9 * sum / 20d;
		scaleThicknessD = Math.min(scaleThicknessD, max);
		clawSharpnessD = Math.min(clawSharpnessD, max);
		wingStrengthD = Math.min(wingStrengthD, max);
		fireBreathD = Math.min(fireBreathD, max);
		
		sum = scaleThicknessD + clawSharpnessD + wingStrengthD + fireBreathD;
		//wingStrengthD = (9d/20d) * sum;
		
		return normalizeDragonStats(scaleThicknessD, clawSharpnessD, wingStrengthD, fireBreathD);
	} */

}
