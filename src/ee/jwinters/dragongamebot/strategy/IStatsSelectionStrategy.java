package ee.jwinters.dragongamebot.strategy;

import ee.jwinters.dragongamebot.msg.DragonStatsSolution; 
import ee.jwinters.dragongamebot.msg.Knight;
import ee.jwinters.dragongamebot.msg.Weather;

/**
 * Game strategy - what stats to select for dragon 
 * @author Joonatan
 */
public interface IStatsSelectionStrategy {

	/**
	 * select stats for dragon
	 * @param knight - opposing knight
	 * @param weatherForGame - weather during fights
	 * @return
	 */
	DragonStatsSolution solve(Knight knight, Weather weatherForGame);

}
