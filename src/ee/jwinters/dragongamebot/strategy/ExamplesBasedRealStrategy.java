package ee.jwinters.dragongamebot.strategy;

import java.io.File;
import java.io.IOException;

import ee.jwinters.dragongamebot.msg.DragonStatsSolution;
import ee.jwinters.dragongamebot.msg.Knight;
import ee.jwinters.dragongamebot.msg.Weather;

/**
 * examples based/fixed strategy for real game
 * @author Joonatan
 *
 */
public class ExamplesBasedRealStrategy implements IStatsSelectionStrategy {

	private final WeatherStrategy weatherStrat;

	public ExamplesBasedRealStrategy() throws IOException {
		IStatsSelectionStrategy normal = new ClosestExampleStrategy(new File("./bak/real_analysis/normal_weather_data.txt"));
		IStatsSelectionStrategy rain = new ClosestExampleStrategy(new File("./bak/real_analysis/rain_weather_data.txt"));
		IStatsSelectionStrategy storm = new FixedStrategy(5, 5, 5, 5);
		IStatsSelectionStrategy dry = new FixedStrategy(5, 5, 5, 5);
		IStatsSelectionStrategy fog = new ClosestExampleStrategy(new File("./bak/real_analysis/fog_weather_data.txt"));
		weatherStrat = new WeatherStrategy(normal,storm,rain,dry,fog);
	}
	
	@Override
	public DragonStatsSolution solve(Knight knight, Weather weatherForGame) {
		return weatherStrat.solve(knight, weatherForGame);
	}

}
