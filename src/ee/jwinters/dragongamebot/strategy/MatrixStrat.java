package ee.jwinters.dragongamebot.strategy;

import ee.jwinters.dragongamebot.msg.DragonStatsSolution;
import ee.jwinters.dragongamebot.msg.Knight;
import ee.jwinters.dragongamebot.msg.Weather;
import ee.jwinters.dragongamebot.utils.DragonMath;

/**
 * Strategy that selects stats by multiplying knight's stats with a matrix
 * @author Joonatan
 *
 */
public class MatrixStrat implements IStatsSelectionStrategy {

	private double[][] core;

	/**
	 * @param core - the matrix to use when solving
	 */
	public MatrixStrat(double[][] core) {
		this.core = core;
	}

	/**
	 * select stats for dragon
	 */
	@Override
	public DragonStatsSolution solve(Knight knight, Weather weatherForGame) {
		double[] stats = DragonMath.multVectorMatrix(knight.getStatsVector(), core);
		return DragonMath.normalizeDragonSolution(stats[2], stats[0], stats[3], stats[1]);
	}

}
