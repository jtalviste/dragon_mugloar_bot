package ee.jwinters.dragongamebot.strategy;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import ee.jwinters.dragongamebot.msg.DragonStatsSolution;
import ee.jwinters.dragongamebot.msg.Knight;
import ee.jwinters.dragongamebot.msg.Weather;
import ee.jwinters.dragongamebot.utils.TrainingDataUtils;

/**
 * Stats selection strategy that uses a set of saved wins. Finds the closest match. 
 * @author Joonatan
 */
public class ClosestExampleStrategy implements IStatsSelectionStrategy {
	
	private List<TrainingData> examples;

	/**
	 * initialize with training data
	 */
	public ClosestExampleStrategy(List<TrainingData> examples) {
		this.examples = examples;
		if(examples.size() == 0) {
			throw new NullPointerException("will not find any examples from empty list");
		}
	}
	
	/**
	 * initialize with training data from file
	 * @throws IOException 
	 */
	public ClosestExampleStrategy(File fileWithExamples) throws IOException {
		this(parseExampleFile(fileWithExamples));
	}

	private static List<TrainingData> parseExampleFile(File fileWithExamples) throws IOException {
		Stream<String> lines = Files.lines(fileWithExamples.toPath());
		Stream<TrainingData> trainingDatas = lines.map(TrainingDataUtils::parseTrainingLine);
		List<TrainingData> ret = trainingDatas.collect(Collectors.toList());
		lines.close();
		return ret;
	}

	/**
	 * select stats for game
	 */
	@Override
	public DragonStatsSolution solve(Knight exampleKnight, Weather weatherForGame) {
		
		Function<TrainingData, Double> distance2 = (td)->{
			return td.knight.calcDistance2From(exampleKnight);
		};
		
		Comparator<TrainingData> distanceComparator = (td1,td2)->{
			return (int) Math.signum(distance2.apply(td1) - distance2.apply(td2));
		};
		
		TrainingData bestExample = examples.stream().min(distanceComparator).get();
		return new DragonStatsSolution(bestExample.dragon);
	}

}
