package ee.jwinters.dragongamebot.msg;

import java.text.ParseException;

/**
 * a game result message 
 * @author Joonatan
 */
public class GameResultResponse {
	private boolean isFake = false;
	private boolean isFakeWin;
	
	public String status;
	public String message;

	public GameResultResponse(boolean isWin) {
		isFake = true;
		isFakeWin = isWin;
	}
	
	public GameResultResponse() {
	}
	
	public boolean isWin() throws ParseException {
		if(isFake) {
			return isFakeWin;
		}
		if(status.equals("Defeat")) {
			return false;
		}
		if(status.equals("Victory")) {
			return true;
		}		
		throw new ParseException("unimplemented parsing of: "+status, 0);
	}
}
