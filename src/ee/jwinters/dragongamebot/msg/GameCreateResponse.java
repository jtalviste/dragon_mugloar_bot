package ee.jwinters.dragongamebot.msg;

/**
 * a game create response
 * @author Joonatan
 *
 */
public class GameCreateResponse {
	public GameCreateResponse(long gameId, Knight knight) {
		this.gameId = gameId;
		this.knight = knight;
	}
	
	public GameCreateResponse() {
	}
	
	public long gameId;
	public Knight knight;
}
