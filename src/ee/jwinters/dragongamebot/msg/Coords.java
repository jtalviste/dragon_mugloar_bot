package ee.jwinters.dragongamebot.msg;

/**
 * part of weather response
 * @author Joonatan
 *
 */
public class Coords {

	public double x;
	public double y;
	public double z;

}
