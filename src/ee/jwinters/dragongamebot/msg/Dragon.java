package ee.jwinters.dragongamebot.msg;

/**
 * a dragon that fights the knight 
 * @author Joonatan
 */
public class Dragon {
    public final int scaleThickness;
    public final int clawSharpness;
    public final int wingStrength;
    public final int fireBreath;
    
	public Dragon(int clawSharpness, int fireBreath, int scaleThickness, int wingStrength) {
		super();
		this.clawSharpness = clawSharpness;
		this.fireBreath = fireBreath;
		this.scaleThickness = scaleThickness;
		this.wingStrength = wingStrength;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dragon other = (Dragon) obj;
		if (clawSharpness != other.clawSharpness)
			return false;
		if (fireBreath != other.fireBreath)
			return false;
		if (scaleThickness != other.scaleThickness)
			return false;
		if (wingStrength != other.wingStrength)
			return false;
		return true;
	}
    
    
}
