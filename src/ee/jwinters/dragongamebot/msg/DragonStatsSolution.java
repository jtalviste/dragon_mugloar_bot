package ee.jwinters.dragongamebot.msg;

/**
 * solution sent to play a game - contains the dragon that fights
 * @author Joonatan
 *
 */
public class DragonStatsSolution {
	public final Dragon dragon;

	public DragonStatsSolution(Dragon dragon) {
		super();
		this.dragon = dragon;
	}
	
	
}
