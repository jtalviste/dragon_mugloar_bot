package ee.jwinters.dragongamebot.msg;

import java.text.ParseException;

/**
 * types of weather
 * @author Joonatan
 *
 */
public enum Weather {
	NORMAL, STORM, RAIN, DRY, FOG;
	
	public int isNormal() {
		return this == NORMAL ? 1 : 0;
	}

	public int isStorm() {
		return this == STORM  ? 1 : 0;
	}

	public int isRain() {
		return this == RAIN  ? 1 : 0;
	}

	public int isDry() {
		return this == DRY  ? 1 : 0;
	}

	public int isFog() {
		return this == FOG  ? 1 : 0;
	}
	
	/**
	 * parses a weather API response
	 * @param weatherResponse - the response to parse 
	 * @return
	 * @throws ParseException
	 */
	public static Weather toWeather(WeatherResponse weatherResponse) throws ParseException {
		if(weatherResponse.code.equals("NMR")) {
			return NORMAL;
		}
		if(weatherResponse.code.equals("SRO")) {
			return STORM;
		}
		if(weatherResponse.code.equals("HVA")) {
			return RAIN;
		}
		if(weatherResponse.code.equals("T E")) {
			return DRY;
		}
		if(weatherResponse.code.equals("FUNDEFINEDG")) {
			return FOG;
		}
		
		
		throw new ParseException("Don't understand weather response with code "+weatherResponse.code, 0);
	}
	
	
}
