package ee.jwinters.dragongamebot.msg;


import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Knight - part of game reation response
 * @author Joonatan
 *
 */
public class Knight {
	public Knight(int agility, int armor, int attack, int endurance) {
		super();
		this.agility = agility;
		this.armor = armor;
		this.attack = attack;
		this.endurance = endurance;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Knight other = (Knight) obj;
		if (agility != other.agility)
			return false;
		if (armor != other.armor)
			return false;
		if (attack != other.attack)
			return false;
		if (endurance != other.endurance)
			return false;
		return true;
	}

	public Knight() {
	}
	
	public String name;
	public int attack;
	public int armor;
	public int agility;
	public int endurance;
	
	@JsonIgnore
	public double[] getStatsVector() {
		return new double[] {agility, armor, attack, endurance};
	}

	public double calcDistance2From(Knight otherKnight) {
		return Math.pow(otherKnight.agility - agility, 2)+
				Math.pow(otherKnight.armor - armor, 2)+
				Math.pow(otherKnight.attack - attack, 2)+
				Math.pow(otherKnight.endurance - endurance, 2);
	}

	
}
