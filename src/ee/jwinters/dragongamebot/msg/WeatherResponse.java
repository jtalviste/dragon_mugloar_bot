package ee.jwinters.dragongamebot.msg;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 * Weather API XML response
 * @author Joonatan
 *
 */
@XmlRootElement(name="report")
public class WeatherResponse {

	public Coords coords;
	public Object time;
	public String code;
	public String message;
	
	@JacksonXmlProperty(localName = "varX-Rating")
	public int varXRating;
	
}
