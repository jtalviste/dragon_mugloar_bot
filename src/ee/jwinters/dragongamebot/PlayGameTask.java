package ee.jwinters.dragongamebot;

import java.io.IOException;
import java.text.ParseException;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;

import ee.jwinters.dragongamebot.msg.Dragon;
import ee.jwinters.dragongamebot.msg.DragonStatsSolution;
import ee.jwinters.dragongamebot.msg.GameCreateResponse;
import ee.jwinters.dragongamebot.msg.GameResultResponse;
import ee.jwinters.dragongamebot.msg.Knight;
import ee.jwinters.dragongamebot.msg.Weather;
import ee.jwinters.dragongamebot.rest.IDragonGameClient;
import ee.jwinters.dragongamebot.rest.IWeatherClient;
import ee.jwinters.dragongamebot.strategy.IStatsSelectionStrategy;
import ee.jwinters.dragongamebot.utils.JSONUtils;

/**
 * task for executor that plays one game
 * @author Joonatan
 *
 */
public class PlayGameTask implements Runnable {

	static private Logger LOG = Logger.getLogger(PlayGameTask.class);
	private IDragonGameClient gameClient;
	private IWeatherClient weatherClient;
	private IStatsSelectionStrategy strategy;
	private GameStats stats;

	/**
	 * create a game task
	 * @param stats - the statistics logging client
	 * @param gameClient - the game client 
	 * @param weatherClient - weather client
	 * @param strategy - strategy to use when playing a game
	 * @throws JsonProcessingException
	 */
	public PlayGameTask(GameStats stats, IDragonGameClient gameClient, IWeatherClient weatherClient, IStatsSelectionStrategy strategy)
			throws JsonProcessingException {
		this.stats = stats;
		this.gameClient = gameClient;
		this.weatherClient = weatherClient;
		this.strategy = strategy;
	}

	/**
	 * plays on game
	 */
	@Override
	public void run() {
		boolean won = false;
		boolean errord = false;
		Knight knight = null;
		Weather weather = null;
		Dragon dragon = null;
		try {
			GameCreateResponse gameCreateResponse = gameClient.createGame();
			knight = gameCreateResponse.knight;
			
			weather = weatherClient.getWeather(gameCreateResponse.gameId);
			
			DragonStatsSolution solution = strategy.solve(gameCreateResponse.knight, weather);
			dragon = solution.dragon;
			GameResultResponse gameResult;
			gameResult = gameClient.playGame(gameCreateResponse.gameId, solution);
			
			won = gameResult.isWin();
			LOG.debug("Game Result"
					+ "\n" + JSONUtils.toFormattedJSON(knight)
					+ "\n" + weather
					+ "\n" + JSONUtils.toFormattedJSON(solution.dragon)
					+ "\n" + JSONUtils.toFormattedJSON(gameResult.message)
					+ "\n" + JSONUtils.toFormattedJSON(gameResult.status));
		} catch (JsonProcessingException | ParseException e) {
			e.printStackTrace();
			errord = true;
		} finally {
			try {
				stats.playedGame(won, errord, knight, weather, dragon);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
