package ee.jwinters.dragongamebot;

import java.io.IOException;

import org.apache.log4j.Logger;

import ee.jwinters.dragongamebot.msg.Dragon;
import ee.jwinters.dragongamebot.msg.Knight;
import ee.jwinters.dragongamebot.msg.Weather;
import ee.jwinters.dragongamebot.utils.TrainingDataUtils;

/**
 * utility class to log application progress
 * @author Joonatan
 *
 */
public class GameStats {
	
	private static Logger LOG = Logger.getLogger(GameStats.class);
	
	private int playedCount;
	private int totalGamesToPlay;
	private int winCount;
	private int errorCount;

	private boolean training;
	
	public GameStats(int totalGamesToPlay, boolean doTrain) {
		this.totalGamesToPlay = totalGamesToPlay;
		this.training = doTrain;
	}
	
	/**
	 * print application progress info
	 */
	public void printInfo() {
		int winPrecent = (int)(winCount*100/(playedCount+0.000000001));//so don't divide by zero
		int errorPrecent = (int)(errorCount*100/(playedCount+0.000000001));
		
		int lossCount = playedCount - winCount - errorCount;
		
		String infoString = String.format(
				"Played %s/%s games - %s%% wins (%s%% errors) (%s wins, %s losses, %s errors)",
				playedCount, totalGamesToPlay, winPrecent, errorPrecent, winCount, lossCount, errorCount);
		LOG.info(infoString);
	}

	/**
	 * notification that a game has been played
	 * @param won - whether the game was won
	 * @param errord - true when game resulted in error
	 * @param knight - the knight that fought
	 * @param weather - weather during fight
	 * @param dragon - the dragon that fought
	 * @throws IOException
	 */
	public void playedGame(boolean won, boolean errord, Knight knight, Weather weather, Dragon dragon) throws IOException {
		playedCount++;
		if(won)winCount++;
		if(errord)errorCount++;
		
		if(won && training) {
			TrainingDataUtils.saveGameWin(knight, weather, dragon);
		}
	}

	
	

}
