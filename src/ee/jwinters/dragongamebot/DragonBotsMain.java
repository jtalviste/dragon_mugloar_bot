package ee.jwinters.dragongamebot;

import java.io.IOException;

import org.apache.commons.cli.ParseException;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import ee.jwinters.dragongamebot.strategy.ExamplesBasedRealStrategy;
import ee.jwinters.dragongamebot.strategy.IStatsSelectionStrategy;
import ee.jwinters.dragongamebot.utils.InitLog4J;

/**
 * main entry point to application 
 * 
 * @author Joonatan
 *
 */
public class DragonBotsMain {

	static Logger LOG = Logger.getLogger(DragonBotsMain.class);
	
	/**
	 * @param mainArgs - -N numberOfGames [-train]
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws IOException 
	 */
	public static void main(String[] mainArgs) throws ParseException, InterruptedException, IOException {
		InitLog4J.initLogging();
		
		LOG.warn("Usage arguments: "+CommandLineArgs.USAGE);
		CommandLineArgs args = new CommandLineArgs(mainArgs);
		int numberOfGames = args.numberOfGames;
		boolean train = args.doTrain;
		
		IStatsSelectionStrategy strategy = new ExamplesBasedRealStrategy();
		
		AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(DragonBotHttpConfig.class);
		DragonBotsRunner.playGames(numberOfGames, train, strategy, applicationContext);
	}

}
