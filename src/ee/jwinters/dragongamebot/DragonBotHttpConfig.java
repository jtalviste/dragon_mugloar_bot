package ee.jwinters.dragongamebot;

import java.nio.charset.Charset;
import java.util.Arrays;

import javax.xml.transform.Source;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.http.converter.xml.Jaxb2RootElementHttpMessageConverter;
import org.springframework.http.converter.xml.SourceHttpMessageConverter;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;

import ee.jwinters.dragongamebot.rest.DragonHttpGameClient;
import ee.jwinters.dragongamebot.rest.IDragonGameClient;
import ee.jwinters.dragongamebot.rest.IWeatherClient;
import ee.jwinters.dragongamebot.rest.WeatherHttpClient;

/**
 * creates HTTP game and weather clients using Spring RestTemplate
 * @author Joonatan
 *
 */
@Configuration
public class DragonBotHttpConfig implements IDragonSpringConfig {
	@Autowired
	public RestOperations restTemplate;

	@Override
	@Bean
	public IDragonGameClient gameClient() {
		return new DragonHttpGameClient(restTemplate);
	}
	
	@Override
	@Bean
	public IWeatherClient weatherClient() {
		return new WeatherHttpClient(restTemplate);
	}
	
	@Bean
	public SourceHttpMessageConverter<Source> sourceHttpMessageConverter() {
		return new SourceHttpMessageConverter<Source>();
	}

	@SuppressWarnings("rawtypes")
	@Bean
	public <T> RestTemplate restTemplate(SourceHttpMessageConverter converter) {
		RestTemplate rest = new RestTemplate();
		rest.getMessageConverters().add(0, converter);
		rest.getMessageConverters().add(0, jsonHttpMessageConverter());
		rest.getMessageConverters().add(0, xmlHttpMessageConverter());
		return rest;
	}

	@Bean
	public MappingJacksonHttpMessageConverter jsonHttpMessageConverter() {
	    MappingJacksonHttpMessageConverter converter = new MappingJacksonHttpMessageConverter();
	    converter.setObjectMapper(new ObjectMapper());
	    return converter;
	}

	@Bean
	public Jaxb2RootElementHttpMessageConverter xmlHttpMessageConverter() {
		Jaxb2RootElementHttpMessageConverter converter = new Jaxb2RootElementHttpMessageConverter();
		converter.setSupportedMediaTypes(Arrays.asList(new MediaType("application", "xml", Charset.forName("utf-8"))));
	    return converter;
	}
	
}
