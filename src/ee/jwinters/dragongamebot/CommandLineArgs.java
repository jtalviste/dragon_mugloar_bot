package ee.jwinters.dragongamebot;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * utility class to parse command line
 * @author Joonatan
 *
 */
public class CommandLineArgs {
	
	public static final String USAGE = "-N numberOfGames [-train]";
	
	private final static DefaultParser COMMAND_LINE_PARSER;
	private static final Options COMMAND_LINE_FORMAT;

	static {
		COMMAND_LINE_FORMAT = new Options();
		
		COMMAND_LINE_FORMAT.addOption(new Option("N", true, "Number of games"));
		
        Option opt = new Option("train", false, "Wether to save training data");
        opt.setOptionalArg(true);
        COMMAND_LINE_FORMAT.addOption(opt);
        
        COMMAND_LINE_PARSER = new DefaultParser();
	}
	
	public final int numberOfGames;
	public final boolean doTrain;
	
	
	public CommandLineArgs(String[] args) throws ParseException {
		CommandLine commandLine;
		commandLine = COMMAND_LINE_PARSER.parse(COMMAND_LINE_FORMAT, args);
		
        if (commandLine.hasOption("N"))
        {
            try {
            	this.numberOfGames = Integer.parseInt(commandLine.getOptionValue("N"));
            } catch (NumberFormatException e) {
            	throw new ParseException(e.getMessage());
            }
        } else {
        	throw new ParseException("Didn't find -N argument that says how many games to play");
        }
        
        this.doTrain = commandLine.hasOption("train");
	}
	
}