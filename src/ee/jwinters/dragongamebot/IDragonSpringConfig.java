package ee.jwinters.dragongamebot;

import ee.jwinters.dragongamebot.rest.IDragonGameClient;
import ee.jwinters.dragongamebot.rest.IWeatherClient;

/**
 * interface for spring configurations
 * @author Joonatan
 *
 */
public interface IDragonSpringConfig {

	IDragonGameClient gameClient();

	IWeatherClient weatherClient();

}