package ee.jwinters.dragongamebot.simulation;

import java.io.IOException;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import ee.jwinters.dragongamebot.DragonBotsRunner;
import ee.jwinters.dragongamebot.strategy.IStatsSelectionStrategy;
import ee.jwinters.dragongamebot.utils.InitLog4J;
import ee.jwinters.dragongamebot.utils.TrainingDataUtils;

/**
 * simulated 10k mock games
 * @author Joonatan
 *
 */
public class SimulateGames {
	
	/**
	 * simulation entry point
	 * @param args - unused 
	 * @throws InterruptedException
	 * @throws IOException
	 */
	public static void main(String[] args) throws InterruptedException, IOException {
		TrainingDataUtils.markSimulated();		
		InitLog4J.initLogging();
		
		AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(DragonBotSimulateConfig.class);
		int numberOfGames = 10000;
		IStatsSelectionStrategy strategy = new SimulatedStrategyUsingExamples();
		DragonBotsRunner.playGames(numberOfGames, true, strategy, applicationContext);
	}
	
}

