package ee.jwinters.dragongamebot.simulation;

import org.springframework.context.annotation.Bean;

import ee.jwinters.dragongamebot.IDragonSpringConfig;
import ee.jwinters.dragongamebot.rest.IDragonGameClient;
import ee.jwinters.dragongamebot.rest.IWeatherClient;

/**
 * Mock spring config
 * @author Joonatan
 */
public class DragonBotSimulateConfig implements IDragonSpringConfig {

	@Override @Bean
	public IDragonGameClient gameClient() {
		return MockDragonGame.getDragonGameSimulation();
	}

	@Override @Bean
	public IWeatherClient weatherClient() {
		return MockDragonGame.getDragonGameSimulation();
	}

}
