package ee.jwinters.dragongamebot.simulation;

import static ee.jwinters.dragongamebot.msg.Weather.*;
import static java.lang.Math.pow;

import java.text.ParseException;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicReference;

import com.fasterxml.jackson.core.JsonProcessingException;

import ee.jwinters.dragongamebot.msg.Dragon;
import ee.jwinters.dragongamebot.msg.DragonStatsSolution;
import ee.jwinters.dragongamebot.msg.GameCreateResponse;
import ee.jwinters.dragongamebot.msg.GameResultResponse;
import ee.jwinters.dragongamebot.msg.Knight;
import ee.jwinters.dragongamebot.msg.Weather;
import ee.jwinters.dragongamebot.rest.IDragonGameClient;
import ee.jwinters.dragongamebot.rest.IWeatherClient;
import ee.jwinters.dragongamebot.utils.DragonMath;

/**
 * Mock game client 
 * @author Joonatan
 */
public class MockDragonGame implements IDragonGameClient, IWeatherClient {

	private static AtomicReference<MockDragonGame> INSTANCE = new AtomicReference<MockDragonGame>();

    public MockDragonGame() {
        final MockDragonGame previous = INSTANCE.getAndSet(this);
        if(previous != null)
            throw new IllegalStateException("Second singleton " + this + " created after " + previous);
    }

    public static MockDragonGame getDragonGameSimulation() {
    	if(INSTANCE.get() != null) {
    		return INSTANCE.get();
    	}
    	return new MockDragonGame();
    }
	
    /**
     * object representing one game
     * @author Joonatan
     */
    private class Game {

		private Knight knight;
		private GameCreateResponse createResponse;
		private long gameId;
		private Weather weather;
		private GameResultResponse gameResult;

		public Game(Knight knight, Weather weather) {
			this.knight = knight;
			this.weather = weather;
			gameId = games.size()+1;
			createResponse = new GameCreateResponse(gameId,knight);
		}

		public GameResultResponse play(Dragon dragon) {
			boolean won = decideIfDragonWins(knight,weather,dragon);
			return new GameResultResponse(won);
		}

		private boolean decideIfDragonWins(Knight knight, Weather weather, Dragon dragon) {
			if(weather == STORM) {
				return false;
			}
			
			int dragonHP = 100;
			int knightHP = 100;
			
			int fireAttack = dragon.fireBreath;
			if(weather == RAIN) {
				fireAttack = 0;
			}
			
			int dragonSpeed = dragon.wingStrength - dragon.scaleThickness;
			int knightSpeed = knight.agility + knight.endurance;
			
			int dragonAttack = dragon.clawSharpness + fireAttack;
			int dragonDefence = dragon.scaleThickness + dragon.wingStrength;
			int knightAttack = knight.attack;
			int knightDefence = knight.armor + knightSpeed;
			
			if(weather == RAIN) {
				knightDefence += 10;
			}
			
			if(weather == FOG) {
				dragonAttack += 20;
				dragonSpeed += 20;
			}
			
			if(dragonSpeed > knightSpeed) {
				knightHP -= attackDamage(dragonAttack, knightDefence);
			}
			
			while(dragonHP > 10 && knightHP > 10) {
				dragonHP -= attackDamage(knightAttack, dragonDefence);
				if(dragonHP > 10) {
					knightHP -= attackDamage(dragonAttack, knightDefence);
				}
			}
			
			return dragonHP > knightHP;
		}

		private int attackDamage(int attack, int defence) {
			return (int)(10*pow(0.95, defence)*pow(1.1, attack));
		}

    }
    
    /**
     * mock DB of games - a total memory leak
     */
    private HashMap<Long,Game> games = new HashMap<>();
    
	@Override
	public synchronized GameCreateResponse createGame() {
		Game game = new Game(DragonMath.randomKnight(),DragonMath.randomWeather());
		games.put(game.gameId, game);
		return game.createResponse;
	}

	@Override
	public Weather getWeather(long gameId) throws ParseException {
		return games.get(gameId).weather;
	}

	@Override
	public GameResultResponse playGame(long gameId, DragonStatsSolution solution) throws JsonProcessingException {
		Game game = games.get(gameId);
		GameResultResponse ret = game.gameResult;
		if(ret == null) {
			ret = game.play(solution.dragon);
		}
		return ret;
	}


}
