package ee.jwinters.dragongamebot.simulation;

import ee.jwinters.dragongamebot.msg.DragonStatsSolution;
import ee.jwinters.dragongamebot.msg.Knight;
import ee.jwinters.dragongamebot.msg.Weather;
import ee.jwinters.dragongamebot.strategy.FixedStrategy;
import ee.jwinters.dragongamebot.strategy.IStatsSelectionStrategy;
import ee.jwinters.dragongamebot.strategy.MatrixStrat;
import ee.jwinters.dragongamebot.strategy.RandomStatsStrategy;
import ee.jwinters.dragongamebot.strategy.WeatherStrategy;

/**
 * an example strategy - not a very good one 
 * @author Joonatan
 */
public class SimulatedStrategy implements IStatsSelectionStrategy {

	private RandomStatsStrategy randomStrat = new RandomStatsStrategy();
	
	double[][] normalCore = {
		{0.2,	0.25,	0.25,	0.3},
		{0.2,	0.2,	0.25,	0.4},
		{0.4,	0.4,	0,	0.1},
		{0.2,	0.2,	0.3,	0.25}
	};
	
	double[][] fogCore = {
		{0.3,	0.25,	0.2,	0.3},
		{0.2,	0.25,	0.3,	0.3},
		{0.2,	0.25,	0.25,	0.25},
		{0.2,	0.25,	0.2,	0.3}
	};
	
	private IStatsSelectionStrategy normal = new MatrixStrat(normalCore);;
	private IStatsSelectionStrategy storm = randomStrat;
	private IStatsSelectionStrategy rain = new FixedStrategy(16,1,1,1);
	private IStatsSelectionStrategy dry = new MatrixStrat(normalCore);
	private IStatsSelectionStrategy fog = new MatrixStrat(fogCore);
	
	WeatherStrategy wrappedStrat = new WeatherStrategy(
			normal,storm,rain,dry,fog
			);
	
	@Override
	public DragonStatsSolution solve(Knight knight, Weather weatherForGame) {
		return wrappedStrat.solve(knight, weatherForGame);
	}

}
