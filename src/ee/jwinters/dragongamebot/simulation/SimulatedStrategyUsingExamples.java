package ee.jwinters.dragongamebot.simulation;

import java.io.File;
import java.io.IOException;

import ee.jwinters.dragongamebot.msg.DragonStatsSolution;
import ee.jwinters.dragongamebot.msg.Knight;
import ee.jwinters.dragongamebot.msg.Weather;
import ee.jwinters.dragongamebot.strategy.ClosestExampleStrategy;
import ee.jwinters.dragongamebot.strategy.FixedStrategy;
import ee.jwinters.dragongamebot.strategy.IStatsSelectionStrategy;
import ee.jwinters.dragongamebot.strategy.WeatherStrategy;

/**
 * an example strategy - not a very good one 
 * @author Joonatan
 */
public class SimulatedStrategyUsingExamples implements IStatsSelectionStrategy {

	private final WeatherStrategy weatherStrat;

	public SimulatedStrategyUsingExamples() throws IOException {
		IStatsSelectionStrategy normal = new ClosestExampleStrategy(new File("./bak/simulated_analysis/normal_weather_data.txt"));
		IStatsSelectionStrategy rain = new FixedStrategy(16,1,1,1);
		IStatsSelectionStrategy storm = rain;
		IStatsSelectionStrategy dry = new ClosestExampleStrategy(new File("./bak/simulated_analysis/dry_weather_data.txt"));
		IStatsSelectionStrategy fog = new ClosestExampleStrategy(new File("./bak/simulated_analysis/fog_weather_data.txt"));
		weatherStrat = new WeatherStrategy(normal,storm,rain,dry,fog);
	}
	
	@Override
	public DragonStatsSolution solve(Knight knight, Weather weatherForGame) {
		return weatherStrat.solve(knight, weatherForGame);
	}

}
