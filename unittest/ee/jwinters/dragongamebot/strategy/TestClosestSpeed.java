package ee.jwinters.dragongamebot.strategy;

import static ee.jwinters.dragongamebot.utils.DragonMath.randomDragon;
import static ee.jwinters.dragongamebot.utils.DragonMath.randomKnight;
import static ee.jwinters.dragongamebot.utils.DragonMath.randomWeather;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.jupiter.api.Test;

import ee.jwinters.dragongamebot.utils.InitLog4J;

/**
 * stress test for ClosestExampleStrategy 
 * @author Joonatan
 */
public class TestClosestSpeed {
	
	private static final int EXAMPLE_COUNT_TO_TEST = 5000;
	static Logger LOG = Logger.getLogger(TestClosestSpeed.class);
	
	@Test
	void testClosestStrategySpeed() throws Exception {
		
		InitLog4J.initLogging();
		
		List<TrainingData> examples = new LinkedList<>();
		
		for (int i = 0; i < EXAMPLE_COUNT_TO_TEST; i++) {
			examples.add(new TrainingData(randomKnight(), randomWeather(), randomDragon()));
		}
		
		ClosestExampleStrategy closestExampleStrategy = new ClosestExampleStrategy(examples);
		
		long startTime = System.nanoTime();
		
		int testCount = 100;
		for (int i = 0; i < testCount; i++) {
			closestExampleStrategy.solve(randomKnight(), randomWeather());
		}
		
		long endTime = System.nanoTime();
		long elapsed = endTime - startTime;
		
		String methodCallTime = String.format("%.2f", elapsed / 1000d / 1000d / testCount);
		LOG.info("closest strategy speed is: "+methodCallTime+" ms");
		
	}
}
