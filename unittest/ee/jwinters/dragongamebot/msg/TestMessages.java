package ee.jwinters.dragongamebot.msg;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import ee.jwinters.dragongamebot.msg.GameCreateResponse;
import ee.jwinters.dragongamebot.msg.GameResultResponse;
import ee.jwinters.dragongamebot.msg.Weather;
import ee.jwinters.dragongamebot.msg.WeatherResponse;

/**
 * test JSON and XML serialization
 * @author Joonatan
 *
 */
public class TestMessages {
	
	@Test
	public void testParseGame() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = "{\"gameId\":8825067,\"knight\":{\"name\":\"Sir. Arthur Carson of Northwest Territories\",\"attack\":8,\"armor\":2,\"agility\":5,\"endurance\":5}}";
		
		GameCreateResponse resp = mapper.readValue(jsonInString, GameCreateResponse.class);
		
		assertEquals(resp.gameId, 8825067);
		assertEquals(resp.knight.name, "Sir. Arthur Carson of Northwest Territories");
		assertEquals(resp.knight.endurance, 5);
	}
	
	@Test
	public void testParseWeather() throws Exception {
		XmlMapper mapper = new XmlMapper();
		String xmlInString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><report><time/><coords><x>3916.234</x><y>169.914</y><z>6.33</z></coords><code>NMR</code><message>Another day of everyday normal regular weather, business as usual, unless it�s going to be like the time of the Great Paprika Mayonnaise Incident of 2014, that was some pretty nasty stuff.</message><varX-Rating>8</varX-Rating></report>";
		
		WeatherResponse resp = mapper.readValue(xmlInString, WeatherResponse.class);
		Weather.toWeather(resp);
		
		assertEquals(resp.coords.x, 3916.234, 0.001);
		assertEquals(resp.varXRating, 8);
	}
	
	@Test
	public void testParseGameResult() throws Exception {
		String json = "{\r\n" + 
				"  \"status\" : \"Defeat\",\r\n" + 
				"  \"message\" : \"Dragon could not compete with knights epic armor\"\r\n" + 
				"}";
		ObjectMapper mapper = new ObjectMapper();
		GameResultResponse resp = mapper.readValue(json, GameResultResponse.class);
		assertEquals(resp.isWin(), false);
	}
	
	@Test
	public void testSerialize() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = "{\"gameId\":8825067,\"knight\":{\"name\":\"Sir. Arthur Carson of Northwest Territories\",\"attack\":8,\"armor\":2,\"agility\":5,\"endurance\":5}}";
		
		
		GameCreateResponse resp = mapper.readValue(jsonInString, GameCreateResponse.class);
		
		assertEquals(resp.gameId, 8825067);
		assertEquals(resp.knight.name, "Sir. Arthur Carson of Northwest Territories");
		assertEquals(resp.knight.endurance, 5);
		
		String jsonOutString = toString(resp);
		
		assertEquals(jsonInString, jsonOutString);
	}

	private String toString(GameCreateResponse resp) throws JsonProcessingException {
		String jsonOutString = new ObjectMapper().writeValueAsString(resp);
		return jsonOutString;
	}
	
}
