package ee.jwinters.dragongamebot;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.apache.commons.cli.ParseException;
import org.junit.jupiter.api.Test;

/**
 * test command line argumetns parsing
 * @author Joonatan
 *
 */
public class TestArgsParser {
	
	@Test
	void testEmptyArgs() throws Exception {
		assertThrows(ParseException.class, ()->{
			new CommandLineArgs(new String[] {});
		});
	}
	
	@Test
	void testCorrectArgs100() throws Exception {
		CommandLineArgs args = new CommandLineArgs(new String[] {"-N","100"});
		assertEquals(args.doTrain, false);
		assertEquals(args.numberOfGames, 100);
	}
	
	@Test
	void testCorrectArgs1000() throws Exception {
		CommandLineArgs args = new CommandLineArgs(new String[] {"-N","1000"});
		assertEquals(args.doTrain, false);
		assertEquals(args.numberOfGames, 1000);
	}
	
	@Test
	void testCorrectArgs1000Train() throws Exception {
		CommandLineArgs args = new CommandLineArgs(new String[] {"-N","1000","-train"});
		assertEquals(args.doTrain, true);
		assertEquals(args.numberOfGames, 1000);
	}
	
	@Test
	void testCorrectArgsTrain1000() throws Exception {
		CommandLineArgs args = new CommandLineArgs(new String[] {"-train","-N","1000"});
		assertEquals(args.doTrain, true);
		assertEquals(args.numberOfGames, 1000);
	}
	
	@Test
	void testWrongArgs1() throws Exception {
		assertThrows(ParseException.class, ()->{
			new CommandLineArgs(new String[] {"-N","A"});
		});
	}
	@Test
	void testWrongArgs2() throws Exception {
		assertThrows(ParseException.class, ()->{
			new CommandLineArgs(new String[] {"-N","A","--train"});
		});
	}
	
	@Test
	void testWrongArgs3() throws Exception {
		assertThrows(ParseException.class, ()->{
			new CommandLineArgs(new String[] {"-A","100","--train"});
		});
	}
	
	@Test
	void testWrongArgs4() throws Exception {
		assertThrows(ParseException.class, ()->{
			new CommandLineArgs(new String[] {"-A","100"});
		});
	}
	
	@Test
	void testWrongArgs5() throws Exception {
		assertThrows(ParseException.class, ()->{
			new CommandLineArgs(new String[] {"--A","100"});
		});
	}
	

}
