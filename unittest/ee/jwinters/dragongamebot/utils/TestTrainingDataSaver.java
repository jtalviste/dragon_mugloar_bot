package ee.jwinters.dragongamebot.utils;

import static ee.jwinters.dragongamebot.msg.Weather.NORMAL;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import org.junit.jupiter.api.Test;

import ee.jwinters.dragongamebot.msg.Dragon;
import ee.jwinters.dragongamebot.msg.Knight;
import ee.jwinters.dragongamebot.msg.Weather;
import ee.jwinters.dragongamebot.strategy.TrainingData;
import ee.jwinters.dragongamebot.utils.TrainingDataUtils;

/**
 * test that training data is saved
 * @author Joonatan
 *
 */
public class TestTrainingDataSaver {
	@Test
	void testSaveData() throws Exception {
		Knight knight = new Knight(1,2,3,4);
		Weather weather = NORMAL;
		Dragon dragon = new Dragon(1, 2, 3, 4);
		TrainingDataUtils.saveGameWin(knight, weather, dragon);
		
		try(BufferedReader lineReader = 
				new BufferedReader(
						new InputStreamReader(
								new FileInputStream(TrainingDataUtils.CURRENT_TRAINING_FILE)))){
			assertEquals(lineReader.readLine(), "1,2,3,4,1,0,0,0,0;1,2,3,4");
		}
		
		TrainingDataUtils.CURRENT_TRAINING_FILE.delete();
	}
	
	@Test
	void testReadData() throws Exception {
		Knight knight = new Knight(1, 2, 3, 4);
		Weather weather = NORMAL;
		Dragon dragon = new Dragon(1, 2, 3, 4);
		String trainingDataline = TrainingDataUtils.makeTrainingDataLine(knight, weather, dragon);
		TrainingData trainingData = TrainingDataUtils.parseTrainingLine(trainingDataline);
		assertEquals(knight, trainingData.knight);
		assertEquals(weather, trainingData.weather);
		assertEquals(dragon, trainingData.dragon);
	}
}
