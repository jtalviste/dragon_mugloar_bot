package ee.jwinters.dragongamebot.utils;

import static ee.jwinters.dragongamebot.utils.DragonMath.multVectorMatrix;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

/**
 * test dragon math
 * @author Joonatan
 *
 */
public class TestDragonMath {
	/**
	 * test matrix multiplication
	 * @throws Exception
	 */
	@Test
	void testMMult() throws Exception {
		double[][] dryCore = {
				{0.2207894321,	0.2563073672,	0.2570729606,	0.2658302402},
				{0.2169353965,	0.2240437085,	0.2512572345,	0.3077636605},
				{0.3394794193,	0.4138779191,	0.0884841161,	0.1581585456},
				{0.2240591419,	0.2370517471,	0.2792722423,	0.2596168686}
			};
		
		double[] knightStats = {6,2,8,4};
		double claw = 5.37068;
		double fire = 6.24516;
		double scale = 3.86991;
		double wing = 4.51424;
		double[] actual = multVectorMatrix(knightStats, dryCore);
		assertEquals(claw, actual[0], 0.01);
		assertEquals(fire, actual[1], 0.01);
		assertEquals(scale, actual[2], 0.01);
		assertEquals(wing, actual[3], 0.01);
		
	}
}
